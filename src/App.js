import React, { useRef, useState, useEffect } from "react";
import "./App.css";
import { TradingViewChart } from "nvest-trading-view";
import "nvest-trading-view/dist/index.css";

const App = () => {
  const size = useRef();
  const [xy, setXY] = useState({ x: 0, y: 0 });
  useEffect(() => {
    !size.current.offsetWidth
      ? console.log()
      : setXY({ x: size.current?.offsetWidth, y: size.current?.clientHeight });
  }, [size.current?.offsetWidth]);

  return (
    <div style={{ height: "100vh", width: "100vw" }} ref={size} className="App">
      {xy.x === 0 ? <h1></h1> : <TradingViewChart height={xy.y} width={xy.x} />}
    </div>
  );
};

export default App;
